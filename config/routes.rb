Rails.application.routes.draw do

  get 'dashboard/index'

  devise_for :users
  
  resources :albums do
  	resources :photos
  end	

  resources :users
  root 'dashboard#index'
end
