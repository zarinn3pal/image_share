class AlbumsController < ApplicationController

	def show
		@album = Album.find(params[:id])
		@photos = @album.photos.all
	end
	def new
		if user_signed_in? 
			@album = Album.new	

		else
			redirect_to root_path, notice: 'Please Login to access this page'
		end
		
	end

	def create
		album_param = album_params
		album_param[:user_id] = current_user.id
		@album = Album.new(album_param)


		if @album.save
			redirect_to  album_path(@album.id), notice: 'Album Successfully Created'

		else
			redirect_to action: 'new' 
			flash[:danger] = "Album was not created due to:#{@album.errors.full_messages}"
			
		end
		
	end

	private

		def album_params
			params.require(:album).permit(:name)
			
		end
end



