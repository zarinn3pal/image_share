class PhotosController < ApplicationController


	def new
		@album = Album.find(params[:album_id])
		@photo = Photo.new	
	end

	def create
		photo_param = photo_params
		photo_param[:album_id] = params[:album_id]
		@album = Album.find(params[:album_id])
		@photo = @album.photos.new(photo_param)
		
		if @album.photos_count > 24
			redirect_to album_path(@album.id)
			flash[:danger] = "Album cannot have more than 25 photos"
		else
			if @photo.save
				redirect_to album_path(@album.id), notice: 'Photo Successfully Added'
			else
				redirect_to album_path(@album.id)
				flash[:danger] = "Album was not created due to:#{@photo.errors.full_messages}"
			end	
		end
	end

	private
		def photo_params
			params.require(:photo).permit(:tagline, :album_id, :image)	
		end
end
