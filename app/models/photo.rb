class Photo < ApplicationRecord
	belongs_to :album, counter_cache: true

	has_attached_file :image, styles: { medium: "300x300>" , large: "600x600>" }
	validates_attachment_content_type :image, content_type: /\Aimage\/.*\Z/

	validates :tagline, presence: true
	validates :image, presence: true
	validates :tagline , length: {message: 'should have minimum 6 characters' , minimum: 6}
end
