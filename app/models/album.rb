class Album < ApplicationRecord
	has_many :photos
	belongs_to :user

	validates :name, presence: true
	validates :name, length: { message: 'should have minimum 6 characters', minimum: 6 }

end
