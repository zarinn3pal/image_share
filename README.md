# README
An image sharing app.
To run the app download it to your local machine
You must have Ruby on Rails configured

Go to the root of the application using the following command
	"cd image_share"

Now Install required gems using the following command
	"bundle install" or "bundle"

After the bundle is complete use the following command to migrate the database:
	"rake db:migrate"

Now run the rails server using the following command
	"rails server" or "rails s"

Open the browser and type "http://localhost:3000/"
